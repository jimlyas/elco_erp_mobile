package com.nbs.nucleo.presentation

import android.graphics.drawable.Drawable
import androidx.annotation.CallSuper
import com.nbs.nucleo.utils.extensions.setContentLayout
import com.nbs.nucleo.utils.extensions.setEmptyLayout
import com.nbs.nucleo.utils.extensions.setErrorLayout
import com.nbs.nucleo.utils.extensions.setLoadingLayout
import com.nbs.nucleo.utils.extensions.showDefaultState
import com.nbs.nucleo.utils.extensions.showEmptyState
import com.nbs.nucleo.utils.extensions.showErrorState
import com.nbs.nucleo.utils.extensions.showLoadingState
import com.nbs.nucleosnucleo.presentation.BaseActivity
import com.nbs.nucleox.R
import kotlinx.android.synthetic.main.multistate_layout.msv_root

abstract class MultiStateActivity : BaseActivity() {

    protected abstract val contentLayout: Int
    protected abstract val loadingLayout: Int
    protected abstract val errorLayout: Int
    protected abstract val emptyLayout: Int

    @CallSuper
    override fun initUI() {
        setContentLayout(contentLayout)
        setLoadingLayout(loadingLayout)
        setEmptyLayout(emptyLayout)
        setErrorLayout(errorLayout)
    }

    override val layoutResource = R.layout.multistate_layout

    private fun setContentLayout(layout: Int) {
        msv_root.setContentLayout(layout, layoutInflater)
    }

    private fun setErrorLayout(layout: Int) {
        msv_root.setErrorLayout(layout)
    }

    private fun setLoadingLayout(layout: Int) {
        msv_root.setLoadingLayout(layout)
    }

    private fun setEmptyLayout(layout: Int) {
        msv_root.setEmptyLayout(layout)
    }

    protected open fun showLoadingState() {
        msv_root.showLoadingState()
    }

    protected open fun showDefaultState() {
        msv_root.showDefaultState()
    }

    protected open fun showErrorState(
        errorMessage: String? = null,
        title: String? = null,
        drawable: Drawable? = null,
        errorButton: Pair<String, (() -> Unit)>? = null
    ) {
        msv_root.showErrorState(
            errorMessage = errorMessage,
            title = title,
            drawable = drawable,
            errorButton = errorButton
        )
    }

    protected open fun showEmptyState(emptyMessage: String? = null, drawable: Drawable? = null) {
        msv_root.showEmptyState(emptyMessage, drawable)
    }
}