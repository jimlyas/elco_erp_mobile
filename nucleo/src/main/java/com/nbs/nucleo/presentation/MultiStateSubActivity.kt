package com.nbs.nucleo.presentation

import androidx.annotation.CallSuper
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem

abstract class MultiStateSubActivity : MultiStateActivity() {

    @CallSuper
    override fun initUI() {
        super.initUI()
        if (getToolbar() != null) {
            setupToolbar(getToolbar(), getActivityTitle(), true)
        } else {
            setupToolbar(getActivityTitle(), true)
        }
    }

    abstract fun getToolbar(): Toolbar?

    abstract fun getActivityTitle(): String


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}