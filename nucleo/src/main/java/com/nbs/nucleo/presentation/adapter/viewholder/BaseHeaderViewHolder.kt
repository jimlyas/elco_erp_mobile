package com.nbs.nucleo.presentation.adapter.viewholder

import android.content.Context
import android.os.Bundle
import android.view.View

abstract class BaseHeaderViewHolder<T>(protected var bundle: Bundle,
                                       private val itemView: View,
                                       protected val context: Context) : BaseItemViewHolder<T>(context, itemView, null, null) {
    override fun bind(data: T) {

    }

    abstract fun show()

    fun saveState(bundle: Bundle) {
        this.bundle = bundle
    }
}