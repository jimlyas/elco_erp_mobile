package com.nbs.nucleo.presentation.adapter.viewholder

import android.content.Context
import android.view.View


abstract class BaseListViewHolder<Data>(protected var mContext: Context,
                                        itemView: View) {

    //    all view binding here e.g set text for textview
    abstract fun bind(data: Data)
}
