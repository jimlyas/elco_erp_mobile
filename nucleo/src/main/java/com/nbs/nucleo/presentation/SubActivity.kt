package com.nbs.nucleo.presentation

import androidx.annotation.CallSuper
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.nbs.nucleosnucleo.presentation.BaseActivity

abstract class SubActivity : BaseActivity() {

    @CallSuper
    override fun initUI() {
        if (getToolbar() != null) {
            setupToolbar(getToolbar(), getActivityTitle(), true)
        } else {
            setupToolbar(getActivityTitle(), true)
        }
    }

    abstract fun getToolbar(): Toolbar?

    abstract fun getActivityTitle(): String


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }
}