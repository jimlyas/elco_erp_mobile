package com.nbs.nucleo.presentation


import android.app.Application
import com.nbs.nucleo.utils.ContextProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

/**
 * Created by ghiyatshanif on 2/21/17.
 */

abstract class BaseApplication : Application(){

    override fun onCreate() {
        super.onCreate()

        ContextProvider.initialize(applicationContext)

        startKoin{
            androidContext(this@BaseApplication)
            modules(getDefinedModules())
        }

        initApp()
    }

    abstract fun initApp()

    abstract fun getDefinedModules(): List<Module>
}
