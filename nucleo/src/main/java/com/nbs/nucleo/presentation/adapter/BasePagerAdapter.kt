package com.nbs.nucleo.presentation.adapter


import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

abstract class BasePagerAdapter<Fragment : androidx.fragment.app.Fragment> : androidx.fragment.app.FragmentStatePagerAdapter {
    var fragments: List<Fragment>
    var titles: List<String> = emptyList()

    constructor(fm: androidx.fragment.app.FragmentManager, fragments: List<Fragment>) : super(fm) {
        this.fragments = fragments
    }

    constructor(fm: androidx.fragment.app.FragmentManager, fragments: List<Fragment>, titles: List<String>) : super(fm) {
        this.fragments = fragments
        this.titles = titles
    }

    abstract override fun getItem(position: Int): Fragment

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (titles.size == fragments.size) titles[position] else super.getPageTitle(position)
    }
}
