package com.nbs.nucleo.presentation

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import com.nbs.nucleo.utils.extensions.setEmptyLayout
import com.nbs.nucleo.utils.extensions.setErrorLayout
import com.nbs.nucleo.utils.extensions.setLoadingLayout
import com.nbs.nucleo.utils.extensions.showDefaultState
import com.nbs.nucleo.utils.extensions.showEmptyState
import com.nbs.nucleo.utils.extensions.showErrorState
import com.nbs.nucleo.utils.extensions.showLoadingState
import com.nbs.nucleosnucleo.presentation.BaseFragment
import com.nbs.nucleox.R
import kotlinx.android.synthetic.main.multistate_layout.msv_content
import kotlinx.android.synthetic.main.multistate_layout.msv_root

abstract class MultiStateFragment : BaseFragment() {

    protected abstract val loadingLayout: Int
    protected abstract val errorLayout: Int
    protected abstract val emptyLayout: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.multistate_layout, container, false)
    }

    @CallSuper
    override fun initUI() {
        setContentLayout(layoutResource)
        setLoadingLayout(view!!, loadingLayout)
        setEmptyLayout(view!!, emptyLayout)
        setErrorLayout(view!!, errorLayout)
    }

    private fun setContentLayout(layout: Int) {
        val view = layoutInflater.inflate(layout, null)
        msv_content.addView(view)
    }

    private fun setErrorLayout(view: View, layout: Int) {
        msv_root.setErrorLayout(layout)
    }

    private fun setLoadingLayout(view: View, layout: Int) {
        msv_root.setLoadingLayout(layout)
    }

    private fun setEmptyLayout(view: View, layout: Int) {
        msv_root.setEmptyLayout(layout)
    }

    protected open fun showLoadingState() {
        msv_root.showLoadingState()
    }

    protected open fun showDefaultState() {
        msv_root.showDefaultState()
    }

    protected open fun showErrorState(
        errorMessage: String? = null,
        title: String? = null,
        drawable: Drawable? = null,
        errorButton: Pair<String, (() -> Unit)>? = null
    ) {
        msv_root.showErrorState(
            errorMessage = errorMessage,
            title = title,
            drawable = drawable,
            errorButton = errorButton
        )
    }

    protected open fun showEmptyState(emptyMessage: String? = null, drawable: Drawable? = null) {
        msv_root.showEmptyState(emptyMessage, drawable)
    }
}