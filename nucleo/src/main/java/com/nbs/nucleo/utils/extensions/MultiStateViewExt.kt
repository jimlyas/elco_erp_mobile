package com.nbs.nucleo.utils.extensions

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.kennyc.view.MultiStateView
import com.kennyc.view.MultiStateView.ViewState.CONTENT
import com.kennyc.view.MultiStateView.ViewState.EMPTY
import com.kennyc.view.MultiStateView.ViewState.ERROR
import com.kennyc.view.MultiStateView.ViewState.LOADING
import com.nbs.nucleox.R


fun MultiStateView.setContentLayout(layout: Int, layoutInflater: LayoutInflater) {
    this.addView(layoutInflater.inflate(layout, null, false))
}

fun MultiStateView.setErrorLayout(layout: Int) {
    this.setViewForState(if (layout > 0) layout else R.layout.default_error_layout, ERROR)
}

fun MultiStateView.setLoadingLayout(layout: Int) {
    this.setViewForState(if (layout > 0) layout else R.layout.default_loading_layout, LOADING)
}

fun MultiStateView.setEmptyLayout(layout: Int) {
    this.setViewForState(if (layout > 0) layout else R.layout.default_error_layout, EMPTY)
}

fun MultiStateView.showLoadingState() {
    this.viewState = LOADING
}

fun MultiStateView.showDefaultState() {
    this.viewState = CONTENT
}


fun MultiStateView.showErrorState(errorMessage: String? = null, title: String? = null, drawable: Drawable? = null, errorButton: Pair<String, (() -> Unit)>? = null) {
    this.viewState = ERROR

    errorMessage?.let {
        val tvError = this.getView(ERROR)?.findViewById<TextView>(R.id.tv_error)
        tvError?.text = errorMessage
    }

    title?.let {
        val tvTitle = this.getView(ERROR)?.findViewById<TextView>(R.id.tv_title)
        tvTitle?.text = title
    }

    drawable?.let {
        val imgError = this.getView(ERROR)?.findViewById<ImageView>(R.id.img_error)
        imgError?.setImageDrawable(drawable)
    }

    errorButton?.let { pair ->
        val btnError = this.getView(ERROR)?.findViewById<Button>(R.id.btn_error)
        btnError?.text = pair.first
        btnError?.visible()

        btnError?.onClick { pair.second.invoke() }
    }
}

fun MultiStateView.showErrorState(action: ((View) -> Unit)) {
    this.viewState = ERROR

    val view = this.getView(ERROR)

    view?.let { action.invoke(it) }
}

fun MultiStateView.showEmptyState(emptyMessage: String? = null, drawable: Drawable? = null, title: String? = null) {
    this.viewState = EMPTY

    emptyMessage?.let {
        val tvError = this.getView(EMPTY)?.findViewById<TextView>(R.id.tv_error)
        tvError?.text = emptyMessage
    }

    title?.let {
        val tvTitle = this.getView(EMPTY)?.findViewById<TextView>(R.id.tv_title)
        tvTitle?.text = title
    }

    drawable?.let {
        val imgError = this.getView(EMPTY)?.findViewById<ImageView>(R.id.img_error)
        imgError?.setImageDrawable(drawable)
    }
}

fun MultiStateView.showEmptyState(action: ((View) -> Unit)) {
    this.viewState = EMPTY

    val view = this.getView(EMPTY)

    view?.let { action.invoke(it) }
}