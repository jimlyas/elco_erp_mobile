package com.nbs.nucleo.utils.rx.operators

import com.google.gson.Gson

fun <T> observableApiError(): ObservableApiErrorOperator<T> {
    return ObservableApiErrorOperator(Gson())
}

fun <T> singleApiError(): SingleApiErrorOperator<T> {
    return SingleApiErrorOperator(Gson())
}

fun <T> flowableApiError(): FlowableApiErrorOperator<T> {
    return FlowableApiErrorOperator(Gson())
}

fun <T> maybeApiError(): MaybeApiErrorOperator<T> {
    return MaybeApiErrorOperator(Gson())
}