package com.nbs.nucleo.utils.rx.apihandler

import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonSyntaxException
import com.nbs.nucleo.data.Result
import com.nbs.nucleo.utils.exception.ApiException
import java.io.IOException
import java.net.SocketTimeoutException


fun <T> genericErrorHandler(e: Throwable, result: MutableLiveData<Result<T>>) {
    // TODO: 28/11/18 define a proper Error Message
    when (e) {
        is ApiException -> result.value = Result.fail(e, e.apiError.message)
        is SocketTimeoutException -> result.value = Result.fail(e, "Connection Timeout")
        is IOException -> result.value = Result.fail(e, "Connection IOException")
        is JsonSyntaxException -> result.value = Result.fail(e, "JSON Exception")
        else -> result.value = Result.fail(e, "An unknown error occurred")
    }
}