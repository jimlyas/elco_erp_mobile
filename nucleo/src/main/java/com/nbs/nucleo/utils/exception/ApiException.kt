package com.nbs.nucleo.utils.exception

import com.nbs.nucleo.data.model.ApiError

class ApiException(val apiError: ApiError, override var response: retrofit2.Response<*>) : ResponseException(response)
