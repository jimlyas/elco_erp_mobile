package com.nbs.nucleo.utils

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import io.reactivex.Observable

/**
 * Created by ghiyatshanif on 03/01/18.
 */

private fun formatNumber(phoneNumber: String, format: PhoneNumberUtil.PhoneNumberFormat, mCountryCode: String): String? {
    if (mCountryCode.isEmpty()) {
        throw IllegalStateException("Can't proceed, pass a valid country code")
    }

    val phoneUtil = PhoneNumberUtil.getInstance()

    try {
        val phone = phoneUtil.parse(phoneNumber, mCountryCode)

        return phoneUtil.format(phone, format)
    } catch (e: NumberParseException) {

    }

    return null
}


fun isValidNumber(phoneNumber: String, mCountryCode: String): Boolean {

    if (mCountryCode.isEmpty()) {
        throw IllegalStateException("Can't proceed, pass a valid country code")
    }

    val phoneUtil = PhoneNumberUtil.getInstance()

    return try {
        val phone = phoneUtil.parse(phoneNumber, mCountryCode)

        phoneUtil.isValidNumber(phone)

    } catch (e: NumberParseException) {
        false
    }

}

//return +6287873112903
fun formatNumberToE164(phoneNumber: String, mCountryCode: String): String? {
    return formatNumber(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164, mCountryCode)
}

//return  +62 878-7311-2903
fun formatNumberToInternational(phoneNumber: String, mCountryCode: String): String? {
    return formatNumber(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL, mCountryCode)
}

//return 0878-7311-2903
fun formatNumberToNational(phoneNumber: String, mCountryCode: String): String? {
    return formatNumber(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL, mCountryCode)
}

//return tel:+62-878-7311-2903
fun formatNumberToRFC3966(phoneNumber: String, mCountryCode: String): String? {
    return formatNumber(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.RFC3966, mCountryCode)
}

