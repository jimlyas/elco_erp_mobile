package com.nbs.nucleo.utils

import android.text.format.DateUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ghiyatshanif on 6/5/17.
 */

const val TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"


fun getRelativeTime(unformatted: String): String {
    var formatted: String

    val date = unformatted.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val hours = date[1].split(".000Z".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    formatted = date[0] + " " + hours[0]

    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var d: Date?
    d = try {
        df.parse(formatted)
    } catch (e: ParseException) {
        e.printStackTrace()
        null
    }

    var timePassed: CharSequence = ""
    if (d != null) {
        val epoch = d.time
        timePassed = DateUtils.getRelativeTimeSpanString(epoch, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)
    }

    return timePassed.toString()

}

fun getReadableDate(time: String, dateFormat: String = TIMESTAMP_FORMAT, mLocale: Locale = Locale.US): String {
    //2018-04-24 03:18:10
    val original = SimpleDateFormat(dateFormat, mLocale)
    original.timeZone = TimeZone.getTimeZone("GMT")
    val format = SimpleDateFormat("d MMM yyyy", mLocale)

    return try {
        format.format(original.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

fun getDayOfWeek(time: String, dateFormat: String = TIMESTAMP_FORMAT, mLocale: Locale = Locale.US): String {
    //2018-04-24 03:18:10
    val original = SimpleDateFormat(dateFormat, mLocale)
    original.timeZone = TimeZone.getTimeZone("GMT")
    val format = SimpleDateFormat("EEEE", mLocale)

    return try {
        format.format(original.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

fun getHour(time: String, dateFormat: String = TIMESTAMP_FORMAT, mLocale: Locale = Locale.US): String {
    //2018-04-24 03:18:10
    val original = SimpleDateFormat(dateFormat, mLocale)
    original.timeZone = TimeZone.getTimeZone("GMT")
    val format = SimpleDateFormat("HH:mm", mLocale)

    return try {
        format.format(original.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

fun formatDate(time: String, sourceFormat: String = TIMESTAMP_FORMAT, mLocale: Locale = Locale.US, expectedFormat: String): String {
    //2018-04-24 03:18:10
    val original = SimpleDateFormat(sourceFormat, mLocale)
    original.timeZone = TimeZone.getTimeZone("GMT")
    val format = SimpleDateFormat(expectedFormat, mLocale)

    return try {
        format.format(original.parse(time))
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

