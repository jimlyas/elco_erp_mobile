@file:Suppress("NOTHING_TO_INLINE")

package com.nbs.nucleo.utils.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

inline fun Disposable.addTo(disposable: CompositeDisposable) {
  disposable.add(this)
}
