package com.nbs.nucleo.utils.exception

open class ResponseException(open var response: retrofit2.Response<*>) : RuntimeException()
