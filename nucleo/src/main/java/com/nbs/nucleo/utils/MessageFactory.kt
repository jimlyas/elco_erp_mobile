package com.nbs.nucleo.utils


import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.nbs.nucleox.R


fun showLoadingDialog(context: Context, message: String): ProgressDialog {
    val progressDialog = ProgressDialog(context)
    progressDialog.setMessage(message)
    progressDialog.show()
    //        if (progressDialog.getWindow() != null) {
    //            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    //        }

    progressDialog.isIndeterminate = true
    progressDialog.setCancelable(false)
    progressDialog.setCanceledOnTouchOutside(false)
    return progressDialog
}

fun showCancellabeLoadingDialog(message: String): ProgressDialog {
    val progressDialog = ProgressDialog(ContextProvider.get())
    progressDialog.setMessage(message)
    progressDialog.show()
    if (progressDialog.window != null) {
        progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    progressDialog.isIndeterminate = true
    progressDialog.setCancelable(true)
    progressDialog.setCanceledOnTouchOutside(true)
    return progressDialog
}

fun showToast(message: String) {
    Toast.makeText(ContextProvider.get(), message, Toast.LENGTH_SHORT).show()
}

fun showLongToast(message: String) {
    Toast.makeText(ContextProvider.get(), message, Toast.LENGTH_LONG).show()
}

fun showSnackbarMessage(view: View, message: String) {

    val snackbar = Snackbar.make(view,
            message, Snackbar.LENGTH_SHORT)
    val sbView = snackbar.view
    val textView = sbView
            .findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(ContextCompat.getColor(ContextProvider.get(), R.color.colorAccent))
    snackbar.show()
}

fun showSnackbarMessage(view: View, message: String, actionName: String, actionListener: View.OnClickListener) {
    val snackbar = Snackbar.make(view,
            message, Snackbar.LENGTH_SHORT)
    val sbView = snackbar.view
    val textView = sbView
            .findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(ContextCompat.getColor(ContextProvider.get(), R.color.colorAccent))

    snackbar.setAction(actionName, actionListener)

    snackbar.show()
}

fun showAlertDialog(context: Context, message: String) {
    AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which -> dialog.dismiss() }.show()
}


fun showAlertDialog(context: Context, message: String, positive: String, positiveListener: DialogInterface.OnClickListener) {

    AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener).show()
}

fun showAlertDialog(context: Context, title: String, message: String) {
    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which -> dialog.dismiss() }.show()
}

fun showAlertDialog(context: Context, title: String, message: String, positive: String, positiveListener: DialogInterface.OnClickListener) {

    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener).show()
}

fun showAlertDialog(context: Context, title: String, message: String, positive: String, positiveListener: DialogInterface.OnClickListener, negative: String, negativeListener: DialogInterface.OnClickListener) {

    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener)
            .setNegativeButton(negative, negativeListener)
            .show()
}

