package com.nbs.nucleo.utils.rx.operators

import com.google.gson.Gson
import com.nbs.nucleo.data.model.ApiError
import com.nbs.nucleo.utils.exception.ApiException
import com.nbs.nucleo.utils.exception.ResponseException
import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.disposables.Disposable
import org.reactivestreams.Subscriber
import retrofit2.Response
import java.io.IOException


/**
 * Takes a [retrofit2.Response], if it's successful send it to [Subscriber.onNext], otherwise
 * attempt to parse the error.
 *
 * Errors that conform to the API's error format are converted into an [ApiException] exception and sent to
 * [Subscriber.onError], otherwise a more generic [ResponseException] is sent to [Subscriber.onError].
 *
 * @param <T> The response type.
</T> */

class SingleApiErrorOperator<T>(private val gson: Gson) : SingleOperator<T, Response<T>> {


    override fun apply(observer: SingleObserver<in T>): SingleObserver<in Response<T>> {
        return object : SingleObserver<Response<T>> {

            override fun onSuccess(response: Response<T>) {
                if (!response.isSuccessful) {
                    try {
                        val envelope = gson.fromJson<ApiError>(response.errorBody()!!.string(), ApiError::class.java)
                        observer.onError(ApiException(envelope ?: ApiError(), response))
                    } catch (e: IOException) {
                        observer.onError(ResponseException(response))
                    }

                } else if (response.code() == 204) {
                    val apiError = ApiError(204, "Data Empty", "No Content")
                    observer.onError(ApiException(apiError, response))
                } else {
                    observer.onSuccess(response.body()!!)
                }
            }

            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onError(e: Throwable) {
                observer.onError(e)
            }
        }
    }
}
