package com.nbs.nucleo.utils.rx.operators

import com.google.gson.Gson
import com.nbs.nucleo.data.model.ApiError
import com.nbs.nucleo.utils.exception.ApiException
import com.nbs.nucleo.utils.exception.ResponseException
import io.reactivex.FlowableOperator
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import retrofit2.Response
import java.io.IOException

/**
 * Takes a [retrofit2.Response], if it's successful send it to [Subscriber.onNext], otherwise
 * attempt to parse the error.
 *
 * Errors that conform to the API's error format are converted into an [ApiException] exception and sent to
 * [Subscriber.onError], otherwise a more generic [ResponseException] is sent to [Subscriber.onError].
 *
 * @param <T> The response type.
</T> */

class FlowableApiErrorOperator<T>(private val gson: Gson) : FlowableOperator<T, Response<T>> {

    override fun apply(subscriber: Subscriber<in T>): Subscriber<in Response<T>> {
        return object : Subscriber<Response<T>> {
            override fun onComplete() {
                subscriber.onComplete()
            }

            override fun onSubscribe(s: Subscription?) {
                subscriber.onSubscribe(s)
            }

            override fun onNext(response: Response<T>?) {
                if (response?.isSuccessful == false) {
                    try {
                        val apiError = gson.fromJson<ApiError>(response.errorBody()!!.string(), ApiError::class.java)
                        subscriber.onError(ApiException(apiError ?: ApiError(), response))
                    } catch (e: IOException) {
                        subscriber.onError(ResponseException(response))
                    }
                } else if (response?.code() == 204) {
                    val apiError = ApiError(204, "Data Empty", "No Content")
                    subscriber.onError(ApiException(apiError, response))
                } else {
                    subscriber.onNext(response?.body()!!)
                    subscriber.onComplete()
                }
            }

            override fun onError(t: Throwable?) {
            }
        }
    }
}
