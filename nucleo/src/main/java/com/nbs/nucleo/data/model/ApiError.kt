package com.nbs.nucleo.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by ghiyatshanif on 2/20/17.
 */

data class ApiError(
        @SerializedName("status")
        var statusCode: Int = 0,
        @SerializedName("message")
        var message: String = "",
        @SerializedName("code")
        var errorCode: String = ""
)