package com.nbs.nucleo.data

interface BaseRepository {
  val webService: WebApi?
  val dbService: LocalDb?
}