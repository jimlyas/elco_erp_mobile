package com.nbs.nucleo.data

import androidx.room.Delete
import androidx.room.Insert
import com.nbs.nucleosnucleo.Model

interface SimpleLocalDb<T : Model> : LocalDb {

    // Basic functions, no need to override

    @Insert
    fun save(vararg response: T)

    @Insert
    fun update(vararg response: T)

    @Delete
    fun remove(vararg response: T)


    // Override this method to comply each class needs

    fun get(intId: Int? = 0): T?

    fun getList(): List<T>

    fun remove(intId: Int? = 0)

    fun removeAll()

    fun isItemCached(intId: Int? = 0, strId: String? = ""): Boolean

    fun isItemCacheExpired(intId: Int? = 0, strId: String? = ""): Boolean

    fun isCached(): Boolean

    fun isCacheExpired(): Boolean
}