package com.nbs.nucleo.data.model

import com.google.gson.annotations.SerializedName

class ApiResponse<T> {

    @SerializedName("code")
    val code: String? = null
    @SerializedName("status")
    val status: String? = null
    @SerializedName("data")
    val data: T? = null
}
