package com.elco_erp.android.data.models

data class product_template(
    var id: Int,
    var name: String,
    var qty_available: Float,
    var purchase_ok: Boolean,
    var sale_ok: Boolean
)