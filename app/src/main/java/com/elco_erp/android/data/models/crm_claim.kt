package com.elco_erp.android.data.models

data class crm_claim(
    //    header
    var name: String? = null,
    var date: String? = null,
    var user_id: String? = null,
    var team_id: String? = null,
    var date_deadline: String? = null,
    //    ic_claim reporter
    var partner_id: String? = null,
    var partner_phone: String? = null,
    var email_from: String? = null,
    //    responsibilities
    var user_fault: String? = null,
    var categ_id: String? = null,
    //    description
    var description: String? = null
)
