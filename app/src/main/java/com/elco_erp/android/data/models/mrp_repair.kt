package com.elco_erp.android.data.models

data class mrp_repair(
    var id: Int,
    var product_qty: Int,
    var name: String,
    var partner_id: String,
    var product_id: String,
    var state: String,
    var guarantee_limit: String
)