package com.elco_erp.android.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nbs.nucleo.data.PreferenceManager
import oogbox.api.odoo.OdooClient
import org.koin.android.ext.android.inject

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    val backend by inject<OdooClient>()
    val pref by inject<PreferenceManager>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    companion object {
        val TAG = BaseActivity::class.java.simpleName
    }
}