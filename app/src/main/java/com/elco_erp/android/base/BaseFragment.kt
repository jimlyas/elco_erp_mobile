package com.elco_erp.android.base

import androidx.fragment.app.Fragment
import oogbox.api.odoo.OdooClient

open class BaseFragment : Fragment() {
    lateinit var backend: OdooClient
}