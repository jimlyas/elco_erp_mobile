package com.elco_erp.android.base

import android.content.Context
import androidx.multidex.MultiDex
import com.elco_erp.android.di.odooModule
import com.elco_erp.android.di.preferenceModule
import com.nbs.nucleo.presentation.BaseApplication
import com.nbs.nucleo.utils.ContextProvider
import com.nbs.nucleo.utils.Timber

/**
 * @author Jimly A.
 * Created on 16-Mar-20.
 */

class ElcoApplication : BaseApplication() {

    override fun initApp() {
        Timber.plant(Timber.DebugTree())
        ContextProvider.initialize(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun getDefinedModules() = arrayListOf(
        preferenceModule,
        odooModule
    )
}