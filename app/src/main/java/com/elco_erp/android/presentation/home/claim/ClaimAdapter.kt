package com.elco_erp.android.presentation.home.claim

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.elco_erp.android.R.layout
import com.elco_erp.android.data.models.crm_claim
import com.elco_erp.android.presentation.home.claim.ClaimAdapter.ClaimHolder
import kotlinx.android.synthetic.main.rec_crm_claim.view.tvClaimDate
import kotlinx.android.synthetic.main.rec_crm_claim.view.tvClaimDateDeadline
import kotlinx.android.synthetic.main.rec_crm_claim.view.tvClaimName
import kotlinx.android.synthetic.main.rec_crm_claim.view.tvClaimTeamId
import kotlinx.android.synthetic.main.rec_crm_claim.view.tvClaimUser
import java.util.ArrayList

internal open class ClaimAdapter : Adapter<ClaimHolder>() {
    var listData = ArrayList<crm_claim>()
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) =
        ClaimHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                layout.rec_crm_claim,
                viewGroup,
                false
            )
        )

    override fun onBindViewHolder(holder: ClaimHolder, i: Int) {
        holder.bind(listData[i])
    }

    fun addItem(model: crm_claim) {
        listData.add(model)
        notifyDataSetChanged()
    }

    override fun getItemCount() = listData.size

    internal inner class ClaimHolder(view: View) : ViewHolder(view) {
        fun bind(model: crm_claim) {
            itemView.apply {
                tvClaimName.text = model.name
                tvClaimUser.text = model.user_id
                tvClaimTeamId.text = model.team_id
                tvClaimDate.text = model.date
                tvClaimDateDeadline.text = model.date_deadline
            }
        }
    }
}