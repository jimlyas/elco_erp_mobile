package com.elco_erp.android.presentation.home.repair

import android.annotation.SuppressLint
import android.app.AlertDialog.Builder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.elco_erp.android.R.layout
import com.elco_erp.android.base.BaseFragment
import com.elco_erp.android.data.models.mrp_repair
import kotlinx.android.synthetic.main.f_list_repair.rvRepair
import kotlinx.android.synthetic.main.f_list_repair.sflRepair
import oogbox.api.odoo.OdooClient
import oogbox.api.odoo.client.helper.data.OdooResult
import oogbox.api.odoo.client.helper.utils.OdooFields
import oogbox.api.odoo.client.helper.utils.OdooValues
import oogbox.api.odoo.client.listeners.IOdooResponse

class RepairFragment : BaseFragment {
    private var repairAdapter: RepairAdapter? = null

    @SuppressLint("ValidFragment")
    constructor(backend: OdooClient) {
        this.backend = backend
    }

    constructor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layout.f_list_repair, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        repairAdapter =
            RepairAdapter { eg ->
                var message = ""
                var state = ""
                when (eg.state) {
                    "draft" -> {
                        message = "Do you want to change current item status to confirmed?"
                        state = "confirmed"
                    }
                    "confirmed" -> {
                        message = "Do you want to change current item status to under repair?"
                        state = "under_repair"
                    }
                    "under_repair" -> {
                        message = "Do you want to change current item status to repaired?"
                        state = "done"
                    }
                    "done" -> message = "Current item is being repaired"
                }
                val dlg =
                    Builder(context).setMessage(message)
                if (eg.state == "done") {
                    dlg.setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                } else {
                    val finalState = state
                    dlg.setPositiveButton("Yes") { _, _ ->
                        val values = OdooValues()
                        values.put("state", finalState)
                        backend.write("mrp.repair", arrayOf(eg.id), values, object : IOdooResponse() {
                            override fun onResult(result: OdooResult) {
                                readData()
                            }
                        })
                    }
                        .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
                }
                dlg.create().show()
            }

        rvRepair.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = repairAdapter
        }
        sflRepair.setOnRefreshListener { readData() }
        readData()
    }

    fun readData() {
        repairAdapter?.data?.clear()
        val fields = OdooFields()
        fields.addAll("id", "product_qty", "name", "partner_id", "product_id", "state", "guarantee_limit")
        backend.searchRead("mrp.repair", null, fields, 0, 10, "", object : IOdooResponse() {
            override fun onResult(result: OdooResult) {
                val results = result.records
                for (record in results) {
                    repairAdapter?.addItem(
                        mrp_repair(
                            record.getInt("id"),
                            record.getInt("product_qty"),
                            record.getString("name"),
                            record.getString("partner_id"),
                            record.getString("product_id"),
                            record.getString("state"),
                            record.getString("guarantee_limit")
                        )
                    )
                }
            }
        })
        sflRepair.isRefreshing = false
    }
}