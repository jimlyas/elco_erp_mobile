package com.elco_erp.android.presentation.home

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.elco_erp.android.BuildConfig
import com.elco_erp.android.R.drawable
import com.elco_erp.android.R.id
import com.elco_erp.android.R.layout
import com.elco_erp.android.base.BaseActivity
import com.elco_erp.android.presentation.home.claim.ClaimFragment
import com.elco_erp.android.presentation.home.home.HomeFragment
import com.elco_erp.android.presentation.home.product.ProductFragment
import com.elco_erp.android.presentation.home.repair.RepairFragment
import com.elco_erp.android.presentation.login.LoginActivity
import com.nbs.nucleo.utils.showToast
import kotlinx.android.synthetic.main.a_home.dl
import kotlinx.android.synthetic.main.a_home.nv
import kotlinx.android.synthetic.main.a_home.tbHome
import kotlinx.android.synthetic.main.nav_header.ivUser
import kotlinx.android.synthetic.main.nav_header.tvUserEmail
import kotlinx.android.synthetic.main.nav_header.tvUserName
import oogbox.api.odoo.OdooUser
import oogbox.api.odoo.client.AuthError
import oogbox.api.odoo.client.listeners.AuthenticateListener
import org.jetbrains.anko.startActivity

open class HomeActivity : BaseActivity() {
    private lateinit var fm: FragmentManager
    private lateinit var productFragment: ProductFragment
    private lateinit var repairFragment: RepairFragment
    private lateinit var claimFragment: ClaimFragment
    private lateinit var homeFragment: HomeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.a_home)
        initViews()
        initHomeConnection()
    }

    override fun onStart() {
        super.onStart()
        initFragments()
        onClickNavigation()
    }

    private fun onClickNavigation() {
        nv.setNavigationItemSelectedListener { menuItem ->
            dl.closeDrawers()
            when (menuItem.itemId) {
                id.menu_home -> {
                    nv.setCheckedItem(id.menu_home)
                    tbHome.subtitle = "Home"
                    fm.beginTransaction().replace(id.content_home, homeFragment).commit()
                }
                id.menu_product -> {
                    nv.setCheckedItem(id.menu_product)
                    tbHome.subtitle = "Products"
                    fm.beginTransaction().replace(id.content_home, productFragment).commit()
                }
                id.menu_repair -> {
                    nv.setCheckedItem(id.menu_repair)
                    tbHome.subtitle = "Repair Management"
                    fm.beginTransaction().replace(id.content_home, repairFragment).commit()
                }
                id.menu_claim -> {
                    nv.setCheckedItem(id.menu_claim)
                    tbHome.subtitle = "Claim Management"
                    fm.beginTransaction().replace(id.content_home, claimFragment).commit()
                }
                id.menu_logout -> {
                    pref.saveBoolean("loggedIn", false)
                    startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                    finish()
                }
            }
            true
        }
    }

    private fun initHomeConnection() {
        backend.setOnConnectListener {
            backend.authenticate(
                pref.getString("username"),
                pref.getString("password"),
                pref.getString("database"),
                object : AuthenticateListener {
                    override fun onLoginFail(error: AuthError?) {
                        showToast("Login Failed")
                        startActivity<LoginActivity>()
                    }

                    override fun onLoginSuccess(user: OdooUser) {
                        Glide.with(applicationContext)
                            .load("${BuildConfig.BASE_URL}/web/image/res.partner/${user.partnerId}/image")
                            .into(ivUser)
                        tvUserName.text = user.name
                        tvUserEmail.text = user.username
                    }
                })
        }

        backend.connect()
    }

    private fun initFragments() {
        repairFragment = RepairFragment(backend)
        productFragment = ProductFragment(backend)
        claimFragment = ClaimFragment(backend)
        homeFragment = HomeFragment()
        fm.beginTransaction().replace(id.content_home, homeFragment).commit()
        tbHome.subtitle = "Home"
    }

    private fun initViews() {
        fm = supportFragmentManager
        setSupportActionBar(tbHome)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(drawable.ic_menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> dl.openDrawer(GravityCompat.START)
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private val TAG = HomeActivity::class.java.simpleName
    }
}