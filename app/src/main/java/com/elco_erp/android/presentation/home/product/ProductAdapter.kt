package com.elco_erp.android.presentation.home.product

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.elco_erp.android.BuildConfig
import com.elco_erp.android.R.layout
import com.elco_erp.android.data.models.product_template
import com.elco_erp.android.presentation.home.product.ProductAdapter.ProductHolder
import com.nbs.nucleo.utils.showToast
import kotlinx.android.synthetic.main.rec_product_template.view.ivProductImage
import kotlinx.android.synthetic.main.rec_product_template.view.tvProductName
import kotlinx.android.synthetic.main.rec_product_template.view.tvProductPurchaseOk
import kotlinx.android.synthetic.main.rec_product_template.view.tvProductQty
import kotlinx.android.synthetic.main.rec_product_template.view.tvProductSaleOk
import java.util.ArrayList

internal class ProductAdapter : RecyclerView.Adapter<ProductHolder>() {
    var listData = ArrayList<product_template>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) =
        ProductHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                layout.rec_product_template,
                viewGroup,
                false
            )
        )

    override fun onBindViewHolder(holder: ProductHolder, i: Int) {
        holder.bind(listData[i])
    }

    override fun getItemCount() = listData.size

    fun addItem(newData: product_template) {
        listData.add(newData)
        notifyDataSetChanged()
    }

    internal inner class ProductHolder(itemView: View) : ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(data: product_template) {
            itemView.apply {
                tvProductName.text = data.name
                if (data.purchase_ok) tvProductPurchaseOk.visibility = View.VISIBLE
                if (data.sale_ok) tvProductSaleOk.visibility = View.VISIBLE
                tvProductQty.text = "${data.qty_available}  pcs"
                Glide.with(itemView)
                    .load("${BuildConfig.BASE_URL}/web/image/product.template/${data.id}/image")
                    .into(ivProductImage)

                setOnClickListener { showToast(data.name) }
            }
        }
    }
}