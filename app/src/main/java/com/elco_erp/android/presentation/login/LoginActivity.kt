package com.elco_erp.android.presentation.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.elco_erp.android.R.id
import com.elco_erp.android.R.layout
import com.elco_erp.android.base.BaseActivity
import com.elco_erp.android.presentation.home.HomeActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.a_login.tvPassword
import kotlinx.android.synthetic.main.a_login.tvUsername
import oogbox.api.odoo.OdooUser
import oogbox.api.odoo.client.AuthError
import oogbox.api.odoo.client.listeners.AuthenticateListener

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.a_login)
        backend.connect()
        initLoginConnection()
    }

    private fun initLoginConnection() {
        if (pref.getBoolean("loggedIn")) {
            startActivity(Intent(applicationContext, HomeActivity::class.java))
            finish()
        }
    }

    fun login(view: View?) {
        authenticate(tvUsername.text.toString(), tvPassword.text.toString())
    }

    private fun authenticate(resultUser: String, resultPass: String) {
        backend.authenticate(resultUser, resultPass, "data-elco", object : AuthenticateListener {
            override fun onLoginSuccess(user: OdooUser) {
                pref.saveString("username", resultUser)
                pref.saveString("password", resultPass)
                pref.saveString("database", user.database)
                pref.saveBoolean("loggedIn", true)
                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                finish()
            }

            override fun onLoginFail(error: AuthError) {
                Snackbar.make(findViewById(id.root_login), error.name, Snackbar.LENGTH_SHORT)
                    .show()
            }
        })
    }

    companion object {
        private val TAG = LoginActivity::class.java.simpleName
    }
}