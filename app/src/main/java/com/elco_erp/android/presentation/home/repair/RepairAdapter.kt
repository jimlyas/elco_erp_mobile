package com.elco_erp.android.presentation.home.repair

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.elco_erp.android.R.layout
import com.elco_erp.android.data.models.mrp_repair
import com.elco_erp.android.presentation.home.repair.RepairAdapter.RepairHolder
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairDate
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairId
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairPerson
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairProduct
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairQty
import kotlinx.android.synthetic.main.rec_mrp_repair.view.tvRepairState
import java.util.ArrayList

class RepairAdapter(private val listener: (mrp_repair) -> Unit) : Adapter<RepairHolder>() {
    var data = ArrayList<mrp_repair>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) =
        RepairHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                layout.rec_mrp_repair,
                viewGroup,
                false
            )
        )

    override fun onBindViewHolder(holder: RepairHolder, i: Int) {
        holder.bind(data[i])
    }

    override fun getItemCount() = data.size

    fun addItem(item: mrp_repair) {
        data.add(item)
        notifyDataSetChanged()
    }

    inner class RepairHolder(itemView: View) : ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: mrp_repair) {
            itemView.apply {
                tvRepairId.text = item.name
                tvRepairState.text = "status : " + item.state
                tvRepairPerson.text = item.partner_id
                tvRepairDate.text = item.guarantee_limit
                tvRepairQty.text = item.product_qty.toString() + " pcs"
                tvRepairProduct.text = item.product_id
                setOnClickListener {
                    listener.invoke(item)
                }
            }
        }
    }
}