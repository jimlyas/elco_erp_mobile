package com.elco_erp.android.presentation.home.product

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.elco_erp.android.R.layout
import com.elco_erp.android.base.BaseFragment
import com.elco_erp.android.data.models.product_template
import kotlinx.android.synthetic.main.f_list_product.rvProduct
import kotlinx.android.synthetic.main.f_list_product.sflProduct
import oogbox.api.odoo.OdooClient
import oogbox.api.odoo.client.helper.data.OdooResult
import oogbox.api.odoo.client.helper.utils.OdooFields
import oogbox.api.odoo.client.listeners.IOdooResponse

class ProductFragment : BaseFragment {
    private var productAdapter: ProductAdapter? = null

    @SuppressLint("ValidFragment")
    constructor(backend: OdooClient) {
        this.backend = backend
    }

    constructor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layout.f_list_product, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productAdapter = ProductAdapter()

        rvProduct.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = productAdapter
        }

        sflProduct.setOnRefreshListener { readData() }

        readData()
    }

    private fun readData() {
        productAdapter?.listData?.clear()
        val fields = OdooFields()
        fields.addAll("id", "name", "qty_available", "purchase_ok", "sale_ok")
        backend.searchRead("product.template", null, fields, 0, 10, "", object : IOdooResponse() {
            override fun onResult(result: OdooResult) {
                result.records.forEach { record ->
                    productAdapter?.addItem(
                        product_template(
                            record.getInt("id"),
                            record.getString("name"),
                            record.getFloat("qty_available"),
                            record.getBoolean("purchase_ok"),
                            record.getBoolean("sale_ok")
                        )
                    )
                }
            }
        })
        sflProduct.isRefreshing = false
    }
}