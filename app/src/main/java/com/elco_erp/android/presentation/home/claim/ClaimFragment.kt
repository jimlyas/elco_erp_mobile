package com.elco_erp.android.presentation.home.claim

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.elco_erp.android.R.layout
import com.elco_erp.android.base.BaseFragment
import com.elco_erp.android.data.models.crm_claim
import kotlinx.android.synthetic.main.f_list_claim.rvClaim
import kotlinx.android.synthetic.main.f_list_claim.sflClaim
import oogbox.api.odoo.OdooClient
import oogbox.api.odoo.client.helper.data.OdooResult
import oogbox.api.odoo.client.helper.utils.OdooFields
import oogbox.api.odoo.client.listeners.IOdooResponse

class ClaimFragment : BaseFragment {
    private var claimAdapter: ClaimAdapter? = null

    @SuppressLint("ValidFragment")
    constructor(backend: OdooClient) {
        this.backend = backend
    }

    constructor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layout.f_list_claim, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        claimAdapter = ClaimAdapter()
        rvClaim.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = claimAdapter
        }

        sflClaim.setOnRefreshListener { readData() }
        readData()
    }

    private fun readData() {
        claimAdapter?.listData?.clear()
        val fields = OdooFields().apply {
            addAll("name", "user_id", "team_id", "date", "date_deadline")
        }
        backend.searchRead("crm.claim", null, fields, 0, 10, "", object : IOdooResponse() {
            override fun onResult(result: OdooResult) {
                result.records.forEach { data ->
                    claimAdapter?.addItem(
                        crm_claim(
                            data.getString("name"),
                            data.getString("user_id"),
                            data.getString("team_id"),
                            data.getString("date"),
                            data.getString("date_deadline")
                        )
                    )

                }
            }
        })
        sflClaim.isRefreshing = false
    }
}