package com.elco_erp.android.di

import com.elco_erp.android.BuildConfig
import com.elco_erp.android.utils.AppConstants
import com.google.gson.Gson
import com.nbs.nucleo.data.PreferenceManager
import com.nbs.nucleo.data.PreferenceManagerImpl
import oogbox.api.odoo.OdooClient
import oogbox.api.odoo.OdooClient.Builder
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * @author Jimly A.
 * Created on 16-Mar-20.
 */

const val prefName = "preferenceName"

val preferenceModule = module {
    single(named(prefName)) { AppConstants.PREF_NAME }
    single { Gson() }
    single<PreferenceManager> { PreferenceManagerImpl(get(), get(named(prefName)), get()) }
}

val odooModule = module {
    single<OdooClient> { Builder(get()).setHost(BuildConfig.BASE_URL).build() }
}
